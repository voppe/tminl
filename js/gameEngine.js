/**
 * @author Oppe
 */

//Canvas size constants
var CANVAS_WIDTH = window.innerWidth,
	CANVAS_HEIGHT = window.innerHeight;

//Get the canvas element and its context
var canvas = document.getElementById("canvas");
var canvasCtx = canvas.getContext("2d");

var game;

//Keypress handlers
document.onkeydown = function(event) {
	game.handleKeyDown();
};
document.onkeyup = function(event) {
	game.handleKeyUp();
};

GameEngine = function() {
	var self_ = this;
		
	//PUBLIC VARIABLES
	this.isPaused = true;
	this.isStarted = false;
	
	//PRIVATE PARAMETERS
	var BEAT_TTL = 12;
	var BEAT_COOLDOWN = 20;
	var FLOOR_HEIGHT = CANVAS_HEIGHT/2;
	var PLAYER_X_POSITION = 32;
	var FLOOR_SPEED = 10;
	var PLAYER_JUMP_TOLERANCE = 46;
	var BACKGROUND_COLOR = "rgb(0,0,0)";

	//PRIVATE VARIABLES
	var _lastBeat = 0,
		_currentBeat = 0;
	var _beatAliveTime = 0;
	var _beatCoolDown = 0;
	var _currentFloorIndex = -1;
	var _previousPlatform = 0;
	var _floorArray = new Array();
	var _freqByteData = new Uint8Array();	
	var _combo = 0;
	var _score = 0;
	var _floorTile = new Image();
	var _delayTime = 0;
	var _lastTime = 0;
	var _currentTime = 0;
	var _backgroundFlash = 0;
	var _untilNextBeat = new Array();
	
	//PRIVATE OBJECTS
	var audio = new AudioEngine();
	var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;  
	var player;
	var beatMap = new Array();
	
	//CALLBACK FUNCTIONS
	var beatDetected = function(time, beatPower) {
		if(beatPower > 300) {
			//console.log("BEAT DETECTED");
			_currentBeat = time;
			//_backgroundFlash = 128;
		}
	};
	
	var audioProcessed = function(freqByteData, currentTime) {
		_freqByteData = freqByteData;
		_currentTime = currentTime;
	};
	
	//INITIALIZATION FUNCTIONS	
	function init() {
		//Initialize canvas
		initGraphicEngine();
		
		//Loads floor asset (NOTE: This wasn't loaded into the object itself in order to save memory)
		_floorTile.src = "assets/floor.png";
		
		//Initialize player
		player = new Player(PLAYER_X_POSITION, 0);
		
		//Store into the floor array a new one that where we'll start
		_floorArray.push(new Floor(0));
		_floorArray[0].width = canvas.width;
		startFloor();
		
		//Initialize audio engine
		initAudioEngine();
	};
	
	function initAudioEngine() {
		//Setup callback functions
		audio.onBeatDetected = beatDetected;
		audio.onAudioProcess = audioProcessed;
		
		//Calculate delay time
		_delayTime = (canvas.width-(player.x+PLAYER_JUMP_TOLERANCE*2))/(60*FLOOR_SPEED);
		audio.setDelay(_delayTime);
	};
	
	function initGraphicEngine() {		
		//Set the canvas' size
		canvas.width = CANVAS_WIDTH;
		canvas.height = CANVAS_HEIGHT;
		
		//Clean the screen before anything gets written on it
		cleanScreen();
	};
	
	
	function cleanScreen() {
		//Fill the whole screen with the specified background color
		canvasCtx.fillStyle = BACKGROUND_COLOR;
		canvasCtx.fillRect(0, 0, canvas.width, canvas.height);	
	};
	
	function drawVisualizer() {
		canvasCtx.save();
		
		//We have stored the audio waveform in an array, so we're going to draw it on screen by using rectangles
		canvasCtx.fillStyle = "#FF0000";
		var width = Math.ceil(canvas.width/_freqByteData.length);
		if(width < 1) {
			width = 1;
		}
		for(var i = 0; i < _freqByteData.length; i++) {
			canvasCtx.fillRect(i*width, canvas.height-_freqByteData[i], width, _freqByteData[i]);
		};
		canvasCtx.restore();
	};
	
	function drawText() {
		canvasCtx.save();
			
		//Write the score
		canvasCtx.font = "20pt visitor";
		canvasCtx.textBaseline = "top";
		canvasCtx.textAlign = "left";
		canvasCtx.fillStyle = "#FFFFFF";
		canvasCtx.fillText("Punteggio: "+_score, 0, 0);
		
		//If we have a combo...
		if(_combo > 1) {
			
			//Based on how much we comboed, increase the font size
			var fontSize = 20+_combo;
			fontStr = fontSize+"pt visitor";
			
			//If we reach past 20 combos, bold it  
			if(fontSize > 40) {
				fontStr = "bold "+fontStr;
				
				//If we reach past 40, write it in cursive. Don't increase it any further.
				if(fontSize > 60) {
					fontStr = "italic bold 60pt visitor";
				}
			}
			
			//Then write the combo meter on screen
			canvasCtx.font = fontStr;
			canvasCtx.fillText("Combo: "+_combo, 0, 32);		
		}
		
		//If the player is respawning, write how much it's left until he gets to control the character again so he can prepare
		if(player.respawnTimer > 0) {
			canvasCtx.font = "20 pt visitor";
			canvasCtx.TextAlign = "center";
			canvasCtx.textBaseline = "bottom";
			
			//Draw it on top of the player
			canvasCtx.fillText(player.respawnTimer, player.x, player.y, player.width);		
		}
		
		canvasCtx.restore();
	}
	
	function analyseBeat() {
		//If we don't already have a beat..
		if(_beatAliveTime-- <= 0 ) {
			//If we detected a new beat...
			if(_currentBeat != _lastBeat) {
				//If we're not just stepped outside an old beat...
				if(_beatCoolDown <= 0) {
					//Finally stop the floor generation
					stopFloor();
				} else {
					//Just keep generating the floor
					keepFloor();
				}
			} else {
				//Do we already have a floor? If not start it, otherwise keep generating it. Useful because when we start we have not started a floor yet.
				_currentFloorIndex < 0 ? startFloor() : keepFloor();
			}
		} 
		
		//Now that we decreased the beat's life timer check if the beat ended and we need a new floor
		if(_beatAliveTime <= 0 && _currentFloorIndex <= 0) {
			startFloor();
		}
	};
	
	//UPDATE FUNCTIONS
	function update() {
		if(self_.isStarted) {
			//Tell the browser to call this function again.
			requestAnimationFrame(update);
			
			//If it's not paused..
			if(!self_.isPaused) {
				//Set the background color depending on wheter we're hearing a beat or not. Gives a visual indicator of when to press the jump button.
				if(_untilNextBeat.length > 0) {
					for(var i = 0; i < _untilNextBeat.length; i++) {
						_untilNextBeat[i] -= (_currentTime - _lastTime);						
					}

					if(_untilNextBeat[0] < 0) {
						_untilNextBeat.shift();
					} else {
						_backgroundFlash = 0;
					}
				}
				//If there are no other beats then fade out. Used to give a nice transition at the end of the file or sudden silences.
				if(_backgroundFlash > 0) {
					_backgroundFlash--;
				}
				BACKGROUND_COLOR = "rgb("+_backgroundFlash+",0,0)";
				_lastTime = _currentTime;
				
				//Before drawing anything, wipe everything on screen
				cleanScreen();
				
				//Check if a beat was detected
				analyseBeat();
				
				//Process, update and draw the floor			
				updateFloor();
				
				//Process, update and draw everything that regards the player
				updatePlayer();
				
				//Draw on screen the audio data stored in the frequency array
				drawVisualizer();
				
				//Draw game text (combo meter, score, etc..)
				drawText();
				
				//Momentarily store the current beat time so we can check wheter there has been a beat or not
				_lastBeat = _currentBeat;
			}
		}
	};
	
	function updateFloor() {
		//Update each floor object in the array
		var i;				
		for(i = 0; i<_floorArray.length; i++) {
			_floorArray[i].update(FLOOR_SPEED);
		}
		
		//Remove floors that passed the game area. This way we reuse memory without piling up floors to infinity.
		if(_floorArray.length > 0) {
			//If it's out, remove it. Do this until we have only the necessary floors.
			while(_floorArray[0].isOut()) {
				_floorArray.shift();
				
				//Shift the pointer index by one too, or we'd point to the next one instead.
				_currentFloorIndex--;
			}
		}
		
		//Draw the floor
		for(i = 0; i<_floorArray.length; i++) {
			//Calculate how many times we have to draw the same tile horizontally.
			var tmpWidth = _floorArray[i].width/_floorTile.width;
			
			for(var ix = 0; ix<tmpWidth; ix++) {
				var iy = FLOOR_HEIGHT;
				
				//Draw the tile vertically too.
				while(iy<canvas.height) {
					canvasCtx.drawImage(_floorTile, _floorArray[i].x+(ix*_floorTile.width), iy);
				
					iy += _floorTile.height
				}
			}
		}
	};
	
	function updatePlayer() {
		//Call the player's update function'
		player.update();
		
		//Check if it's out of bounds
		if(player.y > canvas.height) {
			//If he is, respawn him
			player.respawn();
		}
		
		player.inDanger = false;
		
		//Check if he has the floor under him.
		for(var i = 0; i < _floorArray.length-1; i++) {
			if((player.x > (_floorArray[i].x + _floorArray[i].width)) && (player.x+player.width < _floorArray[i+1].x)) {
				//If he does, then he is in danger.
				player.inDanger = true;
				
				//If he's not respawning, then we set a flag that denotes that we just jumped over lava.
				if(player.respawnTimer <= 0) {
					player.dangerFlag = true;
				}
				break;
			}
		}
		
		//If we aren't respawning...
		if( player.respawnTimer == 0) {
			//If the player's about to go past the floor's height..
			if( (player.y + player.height) >= FLOOR_HEIGHT) {
				//If the player is in danger...
				if(player.inDanger) {
					//ATATATTATATTATTA OMAE WA SHINDEIRU (Translator's note: "You're already dead")
					player.isDead = true;
					
					//And reset everything.
					player.dangerFlag = false;
					_combo = 0;
				} else {
					//If he's not already dead...
					if(!player.isDead) {
						//Allow the player to jump and limit it's vertical position to the floor's height.
						player.onFloor = true;
						player.y = FLOOR_HEIGHT-player.height;
						
						//Increase the score. Higher combo, higher increase.
						_score += _combo;
						
						//If the player just jumped over lava increase the combo meter
						if(player.dangerFlag) {
							_combo++;
							player.dangerFlag = false;
						}
					}
				}
			}
		} else {
			//Otherwise just default the player's position to the floor height.
			player.y = FLOOR_HEIGHT - player.height;
			player.ySpeed = 0;
			
			//If the respawn timer reaches zero and we're still in danger, then increase the player's respawn timer for another frame.
			if(player.respawnTimer==1 && player.inDanger) {
				player.respawnTimer++;
			} 
		}
		
		//Draw the player.
		player.draw(canvasCtx);
	};
	
	//FLOOR MANAGEMENT FUNCTIONS
	function startFloor() {	
		//Start again the floor. Push a new object and store it's index in a pointer variable.
		_floorArray.push(new Floor(canvas.width));
		_currentFloorIndex = _floorArray.length-1;
		keepFloor();
		
		//Prevent new beats to get detected so we don't have gaps wider than we can jump.
		//_beatCoolDown = BEAT_COOLDOWN;
	};
	
	function keepFloor() {
		//Increase the floor's width.
		_floorArray[_currentFloorIndex].width += FLOOR_SPEED;
		
		//Decrese the beat detection cooldown.
		if(_beatCoolDown > 0) {
			_beatCoolDown--;
		}
	};
	
	function stopFloor() {
		//Stop the floor generation for BEAT_TTL frames
		_currentFloorIndex = -1;
		_beatAliveTime = BEAT_TTL;
		
		_untilNextBeat.push(_delayTime);
	};
	
	
	//PUBLIC FUNCTIONS
	this.loadFromFile = function(arrayBuffer) {
		//When it's loaded, then start the game
		audio.onAudioLoaded = self_.start;
		
		//Send the file to the audio engine
		audio.loadFromFile(arrayBuffer);
	};
	
	this.loadFromUrl = function(URL) {
		self_.start();
		audio.loadFromUrl(URL);		
	};
	
	//GAMESTATE MANAGEMENT FUNCTIONS
	this.start = function() {
		//If the game's not started, it means we need to call the update function loop
		if(!self_.isStarted) {
			self_.isStarted = true;
			update();
		}
		
		//Start only if the audio is ready to play
		if(audio.readyToPlay) {
			$("body").attr("data-gamestatus", "playing");	
			
			self_.unpause();
			
			//And spawn the player
			player.respawn();
		}
	};
	
	this.unpause = function() {
		//Play the audio
		audio.play();
		
		//Resume the game
		self_.isPaused = false;
	};
	
	this.pause = function() {
		//Pause the audio 
		audio.pause();
		
		//And the game
		self_.isPaused = true;
	};
	
	this.togglePause = function() {
		//Start or pause. Choose your pill.
		if(self_.isPaused) {
			self_.unpause();
		} else {
			self_.pause();
		}
	};
	
	
	//KEYPRESS INTERFACE
	this.handleKeyDown = function(keyString) {
		player.jumpKeyDown = true;
	}
	
	this.handleKeyUp = function(keyString) {
		player.jumpKeyDown = false;
	}
	
	//Call the initialization function
	init();
};

function Player(startX) {
	var self_ = this;
	
	//Public variables
	//Position variables
	this.x = startX;
	this.y = 0;
	this.height = 0;
	this.width = 0;
	this.respawnTimer = 0;
	
	//Status variables
	this.jumpKeyDown = false;
	this.onFloor = false;
	this.isDead = false;
	this.inDanger = false;
	
	//Private parameters
	var gravity = 2;
	var jumpPower = 16;
	var animDuration = 5;
	var respawnTime = 60;
	
	//Private variables
	var animFrame = 0;
	var ySpeed = 0;
	
	//Image variables
	var runSprite = new Array();
	var jumpSprite = new Image();
	var dyingSprite = new Image();
	
	function init() {
		jumpSprite.src = "assets/mario_jump.png";
		for(var i = 0; i<3; i++) {
			runSprite.push(new Image());
			runSprite[i].src = "assets/mario_run"+(i+1)+".png";
		}
		dyingSprite.src = "assets/mario_dying.png";
		
		jumpSprite.onload = function() {
			self_.height = jumpSprite.height;
			self_.width = jumpSprite.width;
		}
	};
	
	this.respawn = function() {
		//Reset the character's position its variables.
		self_.y = 0;
		self_.onFloor = false;
		self_.isDead = false;
		self_.isDying = false;
		ySpeed = 0;
		
		//Set a new respawn timer until the character is controllable and vulnerable again.
		self_.respawnTimer = respawnTime;
	};
	
	this.update = function() {
		//Each frame decrease the respawn timer.
		if(self_.respawnTimer > 0) {
			self_.respawnTimer--;
		};
		
		//Calculate gravity and apply it.
		if(self_.onFloor) {
			if(self_.jumpKeyDown) {
				ySpeed = -jumpPower;
			} else {
				ySpeed = 0;
			}
		} else {
			ySpeed += gravity;
		}
		
		//Apply gravity. Calculate the new position.
		self_.y += ySpeed;
		
		//Reset the onFloor flag. This stays false unless it's set by the game engine upon the character's collision with terrain.
		self_.onFloor = false;
	};
	
	this.draw = function(ctx) {
		//If we're not on the floor then we're jumping. If we're not, we're running. Draw the appropriate image.
		if(!this.onFloor) {
			if(!this.isDead) {
				ctx.drawImage(jumpSprite, self_.x, self_.y);
			} else {
				ctx.drawImage(dyingSprite, self_.x, self_.y);
			}
		} else {
			var at = Math.floor(animFrame++/animDuration);
			ctx.drawImage(runSprite[at], self_.x, self_.y);
			
			//Cycle through the animation sprites.
			if(animFrame >= animDuration*3) {
				animFrame = 0;
			}
		}
	};
	
	init();
}

function Floor(startX) {
	var self_ = this;
	this.x = startX;
	this.width = 0;
	
	this.isOut = function() {
		//Check if it's out of the canvas' bounds. Used to recycle memory, or it'd pile up to infinity.
		if(self_.x + self_.width + 50 < 0) {
			return true;
		} else {
			return false;
		}
	}
	
	this.update = function(movementAmount) {
		//Update the floor's position
		self_.x -= movementAmount;
	};
};

$(document).ready (function() {
	var start = function(url) {
		$("body").attr("data-gamestatus", "loading");
		
		var request = new XMLHttpRequest();
		request.open('GET', url, true);
		request.responseType = 'arraybuffer';
		
		request.onload = function() {
			game.loadFromFile(request.response);
		};
		request.send();
	}
	
	$('#starteasy').click(function() {
		start('assets/madworld.mp3');
	});

	$('#startmedium').click(function() {
		start('assets/caravan.mp3');
	});
	
	$('#starthard').click(function() {
		start('assets/gigi.mp3');
	});

	$('#startcustom').click(function() {
		$('#musicfile').click();
	});
	
	$('#musicfile').change(function(e) {
    	var input = document.getElementById('musicfile');
		
		// Read from file input
		var fr = new FileReader();
		fr.onload = function(e) {
			game.loadFromFile(e.target.result);
		};
		fr.readAsArrayBuffer(input.files[0]);
	});
	
	//Create the new gameEngine variable to use.
	game = new GameEngine();
});

window.addEventListener("keydown", function(e) {
    // space and arrow keys
    if([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
        e.preventDefault();
    }
}, false);