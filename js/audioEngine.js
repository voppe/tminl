/**
 * @author Oppedisano Valerio
 */
	
function AudioEngine() {
	var self_ = this;
	
	//Public variables
	this.isPlaying = false;
	
	//Callback functions
	this.onBeatDetected = null;
	this.onAudioProcess = null;
	this.onAudioLoaded = null;
	this.onTrackFinished = null;
	this.onLoadProgress = null;
	this.onLoadCompleted = null;
	
	var audioCtx;
	
	//Private variables	
	var _historyBuffer = new Array();
	
	//Source node objects
	var source;
	var buffer;
	
	//Graph nodes
	var preAnalyserNode, jsBeatProcessNode, delayNode, postAnalyserNode, jsPassSpectrumNodem, volumeNode;
	
	//Private variables required for pausing and looping
	var _audioStartTime, _playPosition = 0;

	//Private parameters
	var _fftSize = 1024;
	var _historySize = 12;
	var _beatThreshold = 1.5;
	
	function detectBeat(freqByteData, beatDetected) {
		//Calculate the energy produced by the currently reproduced sound wave
		var instantEnergy = 0;
		for (var i = 0; i < freqByteData.length; i++)
		{
			instantEnergy += Math.pow(128 - freqByteData[i],2);
		}
		instantEnergy /= freqByteData.length;
		
		//If the history buffer is full...
		if (_historyBuffer.length >= _historySize) {
			//Calculate the previous average energy
			var localAverageEnergy = 0;
			for(i = 0; i<_historyBuffer.length; i++) {
				localAverageEnergy += _historyBuffer[i];
			}
			localAverageEnergy /= _historyBuffer.length;
			
			//Calculate variance
			var variance = 0;
			var tmpInstant = Math.sqrt(instantEnergy);
			for(var i = 0; i<_historyBuffer.length; i++) {
				var tmpHistory = Math.sqrt(_historyBuffer[i]);
				variance += Math.pow(tmpHistory - tmpInstant, 2);
			}
			variance /= _historyBuffer.length;
			
			//Detect beat
			var beatPower = instantEnergy - (localAverageEnergy * _beatThreshold);
			if (beatPower > 0) {
				/*console.log("===================");
				console.log("var: "+variance);
				console.log("ie: "+instantEnergy);
				console.log("bt: "+_beatThreshold);*/
				beatDetected(audioCtx.currentTime, beatPower);
			}
			
			//Flush the old value
			_historyBuffer.shift();
		} 
		
		//Insert the new energy value in the history buffer
		_historyBuffer.push(instantEnergy);
	};
	
	function processBeat(e) {
		if(self_.isPlaying) {
			//Get left/right input and output arrays.
			var inputArrayL = e.inputBuffer.getChannelData(0);
			var inputArrayR = e.inputBuffer.getChannelData(1);	
		
			//Pass the input onto the next node.
			e.outputBuffer.getChannelData(0).set(inputArrayL);
			e.outputBuffer.getChannelData(1).set(inputArrayR);
			
			//If the callback function for beat detection is set then we need to call the beat detection algorithm
			if(self_.onBeatDetected != null) {
				var freqByteData = new Uint8Array(preAnalyserNode.frequencyBinCount);
			
				preAnalyserNode.getByteTimeDomainData(freqByteData);
		
				detectBeat(freqByteData, self_.onBeatDetected);
			}
		}
	};
	
	function passSpectrum(freqByteData, audioProcess) {
		audioProcess(freqByteData, audioCtx.currentTime);
	};
	
	function processAudio(e) {
		if(self_.isPlaying) {
			// Get left/right input and output arrays
			var inputArrayL = e.inputBuffer.getChannelData(0);
			var inputArrayR = e.inputBuffer.getChannelData(1);
		
			// Set output data.
			e.outputBuffer.getChannelData(0).set(inputArrayL);
			e.outputBuffer.getChannelData(1).set(inputArrayR);
			
			if(self_.onAudioProcess != null) {
				var freqByteData = new Uint8Array(postAnalyserNode.frequencyBinCount);
		
				postAnalyserNode.getByteTimeDomainData(freqByteData);
				
				passSpectrum(freqByteData, self_.onAudioProcess);
			}
		}
	};
	
	function init() {
		//Get the audio context (Web Audio API)
		try {
			audioCtx = new AudioContext();
		} catch(e) {
			try {
				audioCtx = new webkitAudioContext();
			} catch(e) {
				alert("This browser doesn't support HTML5's Web Audio API.\nPlease use a compatible browser like Google Chrome");
				return false;
			}
		}
		
		//Create and initialize the nodes
		createNodes();
	
		//Build the graph out of the previously created nodes
		connectNodes();
	};
	
	function createNodes() {
		preAnalyserNode = audioCtx.createAnalyser();
		preAnalyserNode.fftSize = _fftSize;
		preAnalyserNode.frequencyBinCount = _fftSize/2;
		
		jsBeatProcessNode = audioCtx.createScriptProcessor(_fftSize, 2, 2);
		jsBeatProcessNode.onaudioprocess = processBeat;
		
		volumeNode = audioCtx.createGain();
		
		delayNode = audioCtx.createDelay(5);
		delayNode.delayTime.value = 0;
		
		postAnalyserNode = audioCtx.createAnalyser();
		postAnalyserNode.fftSize = _fftSize;
		postAnalyserNode.frequencyBinCount = _fftSize/2;
		
		jsPassSpectrumNode = audioCtx.createScriptProcessor(_fftSize, 2, 2);
		jsPassSpectrumNode.onaudioprocess = processAudio;
	};
	
	function connectNodes() {
		preAnalyserNode.connect(jsBeatProcessNode);
		jsBeatProcessNode.connect(volumeNode);
		volumeNode.connect(delayNode);
		delayNode.connect(postAnalyserNode); 
		postAnalyserNode.connect(jsPassSpectrumNode);
		jsPassSpectrumNode.connect(audioCtx.destination);
	};
	
	function readySourceNode() {
		//Create a new audio source
		var src = audioCtx.createBufferSource();
		if(buffer) {
			//Make the audio source use the previously defined buffer, if any
			src.buffer = buffer;
		}
		//Connect the newly created node to the origin of the graph
		src.connect(preAnalyserNode);
		
		return src;
	};
	
	self_.loadFromUrl = function(url) {
		stop();
		var xhr = new XMLHttpRequest();
		xhr.open("GET", url, true);
		//audioCtx.createBuffer expects an arraybuffer variable
		xhr.responseType = "arraybuffer";
		
		//Process the newly loaded buffer 
		xhr.onload = function() {
			buffer = audioCtx.createBuffer(xhr.response, false);
			self_.readyToPlay = true;
		}
		
		//Finally send in the request
		xhr.send();
	};
	
	/**
	* Toggles the playback.
	* 
	*/
	self_.togglePlay = function() {
		if(buffer) {
			if(self_.isPlaying) {
				self_.pause();
			} else {
				self_.play();
			}
		}
	};
	
	/**
	* Plays the audio. If it's paused, the playback gets resumed. 
	* 
	*/
	self_.play= function() {
		if (buffer && !self_.isPlaying) {
			//Whenever we call source.noteOff(0) in the pause() and stop() functions, we're actually deconstructing the source node and disconnecting it from the graph. To resume playback we need to create a new one and set it up again.
			source = readySourceNode();
			
			//Web Audio API doesn't have a play/pause function, but it supports playing sounds starting from a specified point for a specified length of time ( sourceNode.noteGrainOn( "WHEN TO START", "WHERE TO START", "HOW LONG TO KEEP PLAYBACK" ) )
			if (_playPosition > 0) {
				var remainingTime = buffer.duration - _playPosition;
				//Start immediately from _playPosition until the end of the file.
				source.noteGrainOn(0, _playPosition, remainingTime);
		    } else {
				//Start immediately
				source.start();
			}
			
			//Store the time at which we started playback in a variable. Used to calculate playback pointer if paused.
			_audioStartTime = audioCtx.currentTime;
			
			self_.isPlaying = true;
		}
	};
	
	/**
	* Completely halts playback. Can't resume it if not from the start.
	* 
	*/
	self_.stop = function() {
		//Pause the playback and reset the playback pointer
		self_.pause();
		_playPosition = 0;
	};
	
	/**
	* Pauses playback. 
	* 
	*/	
	self_.pause = function() {
		if(source) {
			source.noteOff(0);
		}
		//Calculate the playback pointer.
		_playPosition += audioCtx.currentTime - _audioStartTime;
		self_.isPlaying = false;
	};
	
	/**
	* Sets the volume.
	* @param {number} gain It must be a positive value.
	*/	
	self_.setVolume = function(gain) {
		if(gain > 0) {
			volumeNode.gain.value = gain;
		}
	};
	
	/**
	* Sets the delay between analysis and playback. 
	* @param {number} delay It must be passed in seconds.
	*/	
	self_.setDelay = function(delay) {
		delayNode.delayTime.value = delay;
	};
	
	/**
	* Loads the file. 
	* @param {ArrayBuffer} file The newly loaded file.
	*/		
	self_.loadFromFile = function(file) {
		if(source) {
			self_.stop();
		}
		self_.readyToPlay = false;
		if(!audioCtx) {
			init();
		}
		try {
			audioCtx.decodeAudioData(file, function(ret) {
				buffer = ret;				
				self_.readyToPlay = true;
				if(self_.onAudioLoaded) {
					self_.onAudioLoaded();
				}		
			});	//Oh god please work
		} catch(e) {
			console.error(e);	//GOD FUCKING DAMNIT SHIT FUCK TITS
		}
	};
	
	init();
};

